<!DOCTYPE html>
<html>
<head>
	<title>IF Tanımı</title>
	<meta charset="utf-8">
</head>
<body>

<?php 
/* 

if: eğer bunu sağlarsa bunu yap bunu sağlamazsa bunu yap şeklinde çalışır.
==  : eşitlik kontrolü
=== : aynısı ise (!)
>   : büyük ise
<	: küçük ise
>=	: büyük ve eşit ise
<=	: küçük ve eşit ise

ifelse: değilse ne yapılır.

*/
$sayi = 4;

if ($sayi < 5) {
	echo "sayinin değeri 5den küçük";
	
	}	elseif ($sayi > 3) {
			echo "sayinin değeri 3den büyük";
		}
		elseif ($sayi >= 4) {
			echo "sayinin değeri 4e eşit yada büyük";
		}
		else {
			echo "hiçbir koşul olmadı";
		}


// Kısa if kullanımı:
		echo "<br>";
		echo $sayi == '800' ? 'doğru' : 'else kosulu: yanlis';


		echo "<br>";
		echo "<br>";
		$deger = "Hababam"


?>

	<select>
		<option <?php echo $deger=='Elma' ?'selected' : '' ?> >Elma</option> <!-- değer Elma ise seçili gelsin -->
		<option <?php echo $deger=='Armut' ?'selected' : '' ?> >Armut</option> <!-- değer Armut ise seçili gelsin -->
		<option <?php echo $deger=='Hababam' ?'selected' : '' ?>>Hababam</option> <!-- değer Hababam ise seçili gelsin -->
	</select>




</body>
</html>