<!DOCTYPE html>
<html>
<head>
	<title>Recursive Fonksiyonlar</title>
	<meta  charset = "utf-8">
</head>
<body>

<?php
/*
Recursive Fonksiyon kendi kendini çağıran fonksiyon.

 */

$sabitdeger = 1;

function faktoryel($a)
{
	global $sabitdeger;
	if ($a >1) {
		$sabitdeger = $sabitdeger * $a;
		$a--;
		faktoryel($a);
	}
	return $sabitdeger;
}

echo faktoryel(5);

?>

</body>
</html>