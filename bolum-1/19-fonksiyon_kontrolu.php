<!DOCTYPE html>
<html>
<head>
	<title>Fonksiyon Kontrolü</title>
	<meta  charset = "utf-8">
</head>
<body>

<?php
/*
function_exists: Fonksiyon mevcut mu değil mi?
get_defined_functions: Fonksiyonların listesini getir.

 */

if (function_exists("adsoyad")) {
	adsoyad("bahadır","doğru");
} else {
	echo "böyle bir fonksiyon yok";
}


$yaz = get_defined_functions();
echo "<pre>";
print_r($yaz);
echo "</pre>";

?>

</body>
</html>