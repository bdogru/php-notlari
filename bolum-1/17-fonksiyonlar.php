<!DOCTYPE html>
<html>
<head>
	<title>Fonksiyonlar</title>
	<meta  charset = "utf-8">
</head>
<body>

<?php
/*
Fonksiyon bir işlem grubunu girdisi ve çıktısına göre gruplamak demektir.
Fonksiyonlar parametre alırlar. Parametrelerin varsayılan değerleri olabilir.
Fonksiyonlar return ile sonuç dönerler.
 */

$adsoyad = "BAHADIR DOĞRU";

//strlen bir fonksiyondur ve karakter sayısını döner. Girdisi olan metnin sayısını çıktı olarak verir.

echo strlen($adsoyad); // 14 çıktısı döndü.
echo "<br>";

// ŞİMDİ Fonksiyon oluşturalım:

// (a + b) karesini alan bir fonksiyon yazalım.

function aartibkare($a,$b)
{
	echo  ($a + $b) * ($a + $b);
}

// Return değer döndürür.
function xyussu($x,$y,$ussu) {

	return ($x + $y) ^ $ussu;
}

$degisken = 12;

function ekleme($a)
{
	global $degisken; // dışarıdan geldiğini belirtmek gerek.
	return $a + $degisken;
}

aartibkare(4,5); // fonksiyon zaten echo fonksiyonu

echo "<br>";

echo xyussu(5,4,2); // fonksiyon sadece değer döndürüyor o yüzden echoya gerek var.

echo "<br>";

echo ekleme(20);

echo "<hr>";

function adsoyad($ad,$soyad)
{
	return $ad." <^> ".$soyad;
}

echo adsoyad("Bahadır","Doğru");

echo "<hr>";

function adsoyad2($ad,$soyad="Soyadsız")
{
	return $ad." <^> ".$soyad;
}

echo adsoyad2("Bahadır"); //2.parametre girilmedi varsayılan değeri getirecek.


?>

</body>
</html>