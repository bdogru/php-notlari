<!DOCTYPE html>
<html>
<head>
	<title>Diziler</title>
	<meta charset="utf-8">
</head>
<body>

<?php 
/* 
echo yerine print_r kullanıyoruz
güzel göstermek için <pre> taglerini kullanabiliriz.
sort: küçükten büyüğe sıralar.
resort: büyükten küçüğe sıralar.
in_array: dizi içerisinde var mı yok mu?
implode: dizileri bir değişkene tanımlamaya yarar. metinleştirme yapar.
explode: değişkeni parçalayarak dizi haline getirir.
*/

$dizi = array('emrah',"bahadır","tayyip",10,52);
echo "<pre>";
print_r ($dizi);
echo "</pre>";
echo $dizi[0]; echo "<br>";
echo $dizi[1]; echo "<br>";
echo $dizi[2]; echo "<br>";
echo $dizi[3]; echo "<br>";
echo $dizi[4]; echo "<br>";

$dizi = array(12,24,35,15,25,32,6,4,58,5,4,6);

sort($dizi);
echo "<pre>";
print_r ($dizi);
echo "</pre>";
 

$dizi = array("s","d","g","y","a","b");

sort($dizi);
echo "<pre>";
print_r ($dizi);
echo "</pre>";

echo in_array("a", $dizi);
echo "<br>";
echo strlen(in_array("ü", $dizi)); // sonuç yoksa bişi dönmez strlen o sebeple 0 döndü.
echo "<br>";
echo $sonuc = implode(",", $dizi);
echo "<br>";

$zaman = "27-10-2017 19:08";
$sonuc = explode(" ", $zaman); // diziye dönüştü echo edilemez.
echo "<pre>";
print_r ($sonuc);
echo "</pre>";
$tarih = $sonuc[0];
$saat = $sonuc[1];

echo $tarih;
echo "<br>";
echo $saat;

 ?>
</body>
</html>