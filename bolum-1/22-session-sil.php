<!DOCTYPE html>
<html>
<head>
	<title>Session Kullanımı</title>
	<meta  charset = "utf-8">
</head>
<body>

<?php
/*
Session nasıl silinir.
Unset ile sadece bir değişkini silebilirsin.
session_destroy(); ise sessionı kopmple siler.
 */

session_start();

unset($_SESSION['il']); // sadece ili sil.

session_destroy(); // tüm session'ı sil

?>

</body>
</html>