<!DOCTYPE html>
<html>
<head>
	<title>Matematiksel İşlemler</title>
</head>
<body>

<?php 
/* matematiksel işlemler:

+ Toplama
- Çıkarma
/ Bölme
* Çarpma

+= üstüne ekler
-= üzerinden çıkarır
*= üzerinden çarpar
/= üzerinden çarpar

++ Bir ekler
-- Bir eksilt

%2 mod 2 demektir.

*/

$sayi1 = 59;
$sayi2 = 12;
$sayi3 = 23;

echo $sayi1 . "<br>" . $sayi2 . "<br>" . $sayi3;

$toplam = $sayi1 + $sayi2 + $sayi3;

echo "<br>Toplam: ".$toplam;


$cikarma = $sayi1 - $sayi2 - $sayi3;

echo "<br>Çıkarma: ".$cikarma;


$carpma = $sayi1 * $sayi2 * $sayi3;

echo "<br>Çarpma: ".$carpma;


$bolme = $sayi1 / $sayi2 / $sayi3;

echo "<br>Bölme: ".$bolme;

// farklı işlem yapma:

$toplam += 500;
echo "<br>Yeni Toplam: ".$toplam;

$cikarma -= 500;
echo "<br>Yeni Çıkarma: ".$cikarma;

$bolme /= 500;
echo "<br>Yeni Bölme: ".$bolme;

$carpma += 500;
echo "<br>Yeni Çarpma: ".$carpma;

echo "<br>";
echo $sayi2%2; //12'nin mod2 si 0 yani çifttir.


 ?>


</body>
</html>