-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 05, 2018 at 02:19 PM
-- Server version: 5.7.17-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phpnotlari`
--

-- --------------------------------------------------------

--
-- Table structure for table `kullanici`
--

CREATE TABLE `kullanici` (
  `id` int(11) UNSIGNED NOT NULL,
  `ad` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `soyad` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `eposta` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `dogum_tarih` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Dumping data for table `kullanici`
--

INSERT INTO `kullanici` (`id`, `ad`, `soyad`, `eposta`, `dogum_tarih`, `created_at`) VALUES
(1, 'BAHADIR', 'DOĞRU', 'dogrubahadir1@gmail.com', '1991-10-09', '2018-12-04 14:37:52'),
(2, 'Bahadır2', 'Doğru2', 'dogrubahadir@gmail.com', '1992-10-08', '2018-12-04 14:39:46'),
(3, 'Bahadır3', 'Doğru3', 'dogrubahadir@gmail.com', '1992-10-08', '2018-12-04 14:41:07'),
(4, 'Bahadır3', 'Doğru3', 'dogrubahadir@gmail.com', '2020-02-21', '2018-12-04 14:42:05'),
(5, 'Bahadır3', 'Doğru3', 'dogrubahadir@gmail.com', '2020-02-21', '2018-12-04 14:44:52'),
(6, 'Deneme', 'DOGRU', 'dogrubahadir@gmail.com', '0001-02-12', '2018-12-04 14:47:14'),
(7, 'Deneme', 'DOGRU', 'dogrubahadir@gmail.com', '0001-02-12', '2018-12-04 14:47:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kullanici`
--
ALTER TABLE `kullanici`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kullanici`
--
ALTER TABLE `kullanici`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
