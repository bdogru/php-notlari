<!DOCTYPE html>
<html>
<head>
	<title>Hazır Fonksiyonlar</title>
	<meta charset="utf-8">
</head>
<body>

<?php 
/* 
rand: fonksiyonu belirlenen aralıkta sayı üretir
strtolower: fonksiyonu harfleri küçültür.
strtoupper: fonksiyonu harfleri büyültür.
ucwords: Metnin kelimelerinin ilk harflerini büyük yazar.
ucfirst: Metnin ilk harfini büyütür.
strlen: Metnin Karakter sayısını verir boşluklar dahil.
substr: metnin içinden verilen aralığı seçip bir alt metin oluşturur.
*/

echo $sayi = rand(0,10);

echo $yazi = "BEN BİR PHP DERSİNE KAYIT OLDUM";
echo "<br>";
echo  strtolower($yazi);
echo "<br>";
$yazi = "ben bir php dersine kayıt oldum";
echo  strtoupper($yazi);
echo "<br>";
echo ucwords($yazi);
echo "<br>";
echo ucfirst($yazi);
echo "<br>";
echo "\$yazi değişkenindeki karakter sayısı: " . strlen($yazi);
echo "<br>";
echo substr($yazi,0,10)




 ?>


</body>
</html>