<!DOCTYPE html>
<html>
<head>
	<title>Değişkenler</title>
</head>
<body>

<?php 
/* Değişkenler:
değişkenler $ işareti ile tanımlanır.
= işareti ile değer atanır.
metinler " " veya ' ' ile belirtilir. 
$adsoyad eşit değildir $Adsoyad büyük küçük hassasiyeti vardır.
değişkenler rakam ile baslayamaz.
değişken adında boşluk olamaz.
değişken adı türkçe karakter barınamaz. // Hata verir.

*/


$ad = "BAHADIR";
$soyad = "DOĞRU";
$tckimlik = 11111111111;
$adsoyad = $ad . " " . $soyad;

echo $ad;
echo "<br>";
echo $soyad;
echo "<br>";
echo $adsoyad;
echo $adsoyad."<br><br>";
 ?>

<p>Adı: <?php echo $ad  ?></p>
<p>Soyadı: <?php echo $soyad ?></p>
<br>
<p>AdSoyad: <?php echo $adsoyad ?></p>
<br>
<p>TcKimlikNo: <?php echo $tckimlik ?></p>




</body>
</html>