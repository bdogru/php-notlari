<!DOCTYPE html>
<html>
<head>
	<title>Cookie Kullanımı</title>
	<meta  charset = "utf-8">
</head>
<body>

<?php
/*
Cookie nasıl kullanılır oluşturulur.
Cookie " " ile kullanılır.
Cookie Süre arttırma:
strtotime bir stringi time nesnesine dönüştürür.
strtotime("+30 seconds");
strtotime("+1 hours");
strtotime("+1 day");
strtotime("+1 week");

 */
date_default_timezone_set("Europe/Istanbul");

$adsoyad = "Bahadır Doğru";

setcookie("adsoyad",$adsoyad,time()+3600);

setcookie("adsoyad",$adsoyad,strtotime("+1 week"));

echo $_COOKIE["adsoyad"];


?>

</body>
</html>