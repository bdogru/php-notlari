<!DOCTYPE html>
<html>
<head>
	<title>Session Kullanımı</title>
	<meta  charset = "utf-8">
</head>
<body>

<?php
/*
Session nasıl kullanılır.

session_start(); fonksiyonu ile başlar herzaman.
$_SESSION global değişkeni ile dizi olarak tanımlanır.
Post ve Get gibi okunabilir.
 */

session_start();

$_SESSION['adsoyad']="BAHADIR DOĞRU";
$_SESSION['il']="ISTANBUL";
$_SESSION['ilce']="MERTER";


?>

</body>
</html>