<!DOCTYPE html>
<html>
<head>
	<title>While Döngüsü-2</title>
	<meta  charset = "utf-8">
</head>
<body>

<?php
/*
While Döngüsü Kullanımı - 2 
count: dizilerin eleman sayılarını alır.

 */

$dizi  = array("elma","armut","kavun","karpuz","kiraz","vişne","hasan","sinan" );

for ($i=0; $i <= 5 ; $i++) { 
	echo $dizi[$i]; echo " - ";
}

echo "<br>";

// burada while ile kullanımı:

$dizielemansayisi = count($dizi);

echo $dizielemansayisi;

echo "<br>";


$islemno = 1;
$i = 0;
while ( $islemno <= count($dizi)) {
	echo $dizi[$i];
	echo " <br> ";
	$islemno++;
	$i++;
}




?>






</body>
</html>