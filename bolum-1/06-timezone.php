<!DOCTYPE html>
<html>
<head>
	<title>Tarih Saat İşlemleri</title>
	<meta charset="utf-8">
</head>
<body>

<?php 
/* 
date: zamanı getirir.
date_default_timezone_set: timezone tanımlar.
date_default_timezone_get: timezone'u getirir.

*/


echo date("d-m-y h:i:s");
echo "<br>";
$timezonenum = date_default_timezone_get();
echo $timezonenum;
echo "<br>";
date_default_timezone_set("Europe/Istanbul"); // bu time zone ile tarih saati getirir.
$timezonenum = date_default_timezone_get();
echo $timezonenum;
echo "<br>";
echo date("d-m-y h:i:s");
 ?>
</body>
</html>