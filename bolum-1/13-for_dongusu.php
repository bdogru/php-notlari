<!DOCTYPE html>
<html>
<head>
	<title>For Döngüsü</title>
	<meta charset="utf-8">
</head>
<body>

<?php
/*
For Döngüleri:

Yapısı:
for ($i=0; $i < ; $i++) {
# code...
}
Anlamı:
$i=0    : Başlangıç Değeri
$i <     : Koşul
$i++    : Her bir döngüde yapılacak hareket (artış)

 */

for ($i = 0; $i <= 10; $i++) {
    echo "<br>" . $i;
}

//il plakalarını selectbox'a doldurma:

?>
<hr>
<p>Plakalar:</p>
<select>
	<?php
for ($i = 1; $i <= 81; $i++) {?>

			<option value="<?php echo $i ?>"> <?php echo $i ?> </option>
		<?php }
?>
</select>
<hr>

<?php
// For düngüsü ile 100'e kadar sayalım ve tek ve çift olduklarını vurgulayalım.

$cift = 0;
$tek = 0;

	for ($i=0; $i <=100 ; $i++) { 

		if ($i%2==0) {
			echo $i." (Çift)"; echo "<br>";
			$cift++;
		}
		else {
			echo $i." (Tek)"; echo "<br>";
			$tek++;
		}
	}
	echo "<hr>";
	echo "Toplam Çift sayı: ".$cift."<br>";
	echo "Toplam Tek sayı: ".$tek."<br>";
?>


</body>
</html>